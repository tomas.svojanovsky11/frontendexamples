1. Run the code through the validator [validator](https://validator.w3.org/)
2. Replace or add appropriate tags

MDN: [h1-6](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements)  
MDN: [header](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/header)  
MDN: [ol](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol)  
MDN: [p](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p)  
MDN: [section](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section)  
MDN: [main](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main)  
MDN: [footer](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/footer)
