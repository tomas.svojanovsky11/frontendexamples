1. Convert the text inside the <body> tags into an unordered list.
2. Make the text inside each list item a link. The first item should link to cats.html, the second to dogs.html and the third to apples.html.

MDN [ul](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul)  
MDN [ol](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol)  
MDN [dl](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl)
