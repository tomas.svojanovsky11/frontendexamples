1. Inside the <body>, display the image cat.jpg located inside a folder named img.
2. Provide the browser alternative text describing the image.
3. Add a caption that describes the image.

MDN [img](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img)  
MDN [figure](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figure)  
MDN [figcaption](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figcaption)
