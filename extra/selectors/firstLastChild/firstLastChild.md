1. Create a pseudo-class selector that targets the first-child li in .main-nav. Remove all border styles by setting
border to none. Then, set the top-left and bottom-left border-radius to 5px. Watch this related video for a review on
border-radius.
2. Create a new pseudo-class selector that targets the last-child li in .main-nav. Give it a top-right and bottom-right border-radius of 5px.

MDN: [:first-child](https://developer.mozilla.org/en-US/docs/Web/CSS/:first-child)  
MDN: [:last-child](https://developer.mozilla.org/en-US/docs/Web/CSS/:last-child)
