1. Create an attribute selector that targets img elements with a title attribute value of "avatar". Give the elements a border radius of 50%.
2. Next, create a new attribute selector that targets input elements with a type attribute value of password. Then, set the color to #ccc.
3. Write a new attribute selector that targets input elements with a type attribute value of submit. Then, set the background color to #52bab3.
