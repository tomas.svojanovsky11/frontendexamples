1. Set the main headline to a heading level 1 element. Then, place the line of text below the main headline inside opening and closing paragraph tags.
2. Set the subheading to a heading level 2 element and the line of text below it to a paragraph element.
3. set the third heading to a heading level 3 element and the text below it to a paragraph.

MDN: [h1-h6](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements)  
MDN: [p](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p)
