1. Create a child selector that targets li elements that are direct children of .main-nav. Set the display to inline-block and the left margin to 20px.
2. It looks like we need to decrease the space between h2 elements and the paragraph that immediately follows. Create a new selector that targets p elements that are adjacent siblings of an h2. Then, set the top margin to .5em.

[Child combinator](https://developer.mozilla.org/en-US/docs/Web/CSS/Child_combinator)
[Adjacent sibling combinator](https://developer.mozilla.org/en-US/docs/Web/CSS/Adjacent_sibling_combinator)
[General sibling combinator](https://developer.mozilla.org/en-US/docs/Web/CSS/General_sibling_combinator)

* The > combinator targets a direct child of an element  
* The + combinator targets an element's immediate sibling  
* The ~ combinator targets all the specified siblings that follow an element  
